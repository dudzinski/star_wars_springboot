package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class Owner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ownerId;
    private String fullName;

    public Owner(String fullName) {
        this.fullName = fullName;
    }

    public Owner() {
    }

    public String getFullName() {
        return fullName;
    }

    public Integer getOwnerId() {
        return ownerId;
    }
}
