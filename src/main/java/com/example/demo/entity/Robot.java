package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class Robot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer robotId;
    private String name;
    private int generation;
    @ManyToOne
    private Owner owner;

    public Robot() {
    }

    public Robot(String name, int generation, Owner owner) {
        this.name = name;
        this.generation = generation;
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public int getGeneration() {
        return generation;
    }

    public Owner getOwner() {
        return owner;
    }
}
