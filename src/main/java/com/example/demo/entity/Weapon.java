package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class Weapon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer weaponId;
    private String type;
    @ManyToOne
    private Robot robot;

    public Weapon() {
    }

    public Weapon(String type, Robot robot) {
        this.type = type;
        this.robot = robot;
    }

    public String getType() {
        return type;
    }

    public Robot getRobot() {
        return robot;
    }
}
